import psycopg2
from datetime import datetime
import psycopg2.extras
import json
import boto3
import time
import s3fs
import concurrent.futures
import traceback
import secrets


endpoint = 'some_endpoint'

def lambda_handler(event, context):
    _access_key = 'some_key'
    _secret = 'secret'
    _b = json.loads(event['body'])

    _check = check_user(_b)
    if not _check[0]:
        return {'statusCode': 200,
                'body': json.dumps(_check[1])}

    _lambda_client = boto3.client('lambda', aws_access_key_id=_access_key,
                                  aws_secret_access_key=_secret,
                                  region_name="us-east-2")
    _cursor = get_db_connect().cursor()
    _answers = list()
    _nics = get_nics()
    _filename = f'{datetime.now().strftime("%m-%d-%Y-%H-%M")}'
    _filename += f'{secrets.token_urlsafe(7)}.csv'
    _t_name = _filename.replace('.csv', '').replace('-', '_')
    create_temp_table(_t_name, _cursor)
    _counter = 0
    _gl_counter = 0
    _chunk = list()
    _chunk_counter = 0
    for _record in _b['list']:
        _chunk.append(_record)
        _counter += 1
        _gl_counter += 1
        if _counter == 75:
            _payload = json.dumps({'list': _chunk,
                                       'email': _check[1],
                                        'filename': _filename,
                                        'token': _check[2],
                                       'nicknames': _nics}).encode('utf-8')
            _resp = _lambda_client.invoke(FunctionName='process_chunk',
                                           InvocationType='Event',
                                         Payload=_payload)
            _chunk_counter += 1
            _counter = 0
            _chunk = list()
    if _counter <75 or _gl_counter <75:
        _payload = json.dumps({'list': _chunk,
                                   'email': _check[1],
                                   'filename': _filename,
                                   'token': _check[2],
                                   'nicknames': _nics}).encode('utf-8')
        _resp = _lambda_client.invoke(FunctionName='process_chunk',
                              InvocationType='Event',
                              Payload=_payload,
                              LogType='Tail')
        _chunk_counter += 1

    s3 = s3fs.S3FileSystem(anon=False, key=_access_key,
                           secret=_secret)

    # wait until all records processed
    time.sleep(5)
    print(_chunk_counter)
    while True:
        _cursor.execute(f"select count(*) from usso.t{_t_name}")
        _n = _cursor.fetchone()[0]
        if _n < _gl_counter:
            print()
            print(_n)
            time.sleep(3)
        else:
            break
    _cursor.execute(f"""select * from usso.t{_t_name} """)
    _all = _cursor.fetchall()
    _rec_list = list()
    for _a in  _all:
        _r_w = [' ' if _el is None
                else _el.replace('"', '""') for _el in _a]
        _rec_list.append(_r_w)
    _all_b = ['"' + '","'.join(_a) + '"' for _a in _rec_list]
    _all_bytes = ','.join(get_header()
                                ) + '\n' + '\n'.join(_all_b)

    # -----------------------
    # Check if count-flag is present, if so invoke count function
    # and returns a result
    if "count-flag" in _b and _b["count-flag"].lower() == 'true':
        _count_value = 0
        for _rec in _rec_list:
            if _rec[5] != 'Not found':
                _count_value += 1
        drop_temp_table(_t_name, _cursor)
        _cursor.close()
        return {"statusCode": 200,
                 'body': json.dumps({'Count': _count_value})
                 }
    # ------------------------------------------------

    if 'output_type' not in _b or _b['output_type'  # csv format
                    ].lower() != 'json':
        with s3.open(f'usso/{_check[1]}/{_filename}',
                 'ab') as _s3_csv:
            _s3_csv.write(_all_bytes.encode('utf-8'))

        _s3 = boto3.resource('s3')
        object_acl = _s3.ObjectAcl('usso', f'{_check[1]}/{_filename}')
        response = object_acl.put(ACL='public-read')

        _responce = {"statusCode": 200,
                 'body': f'certain file URL in S3'
                 }
    else:  # json format
        _responce = process_json(_rec_list)
    drop_temp_table(_t_name, _cursor)
    _cursor.close()
    return _responce


def process_json(_rec_list):
    return {"statusCode": 200,
                 'body': json.dumps({'list': _rec_list})
                 }


def get_nics():
    with open('Hyproconisms-May-21-2020.txt') as _csv:
        _nnn = _csv.read().split('\n')
    return [_n.split('\t') for _n in _nnn]


def get_db_connect():
    _conn = psycopg2.connect(
        host="host",
        database="postgres", user="postgres",
        password="pwd")
    _conn.autocommit = True
    return _conn


def check_user(_body):
    _token = _body["token"]
    _cursor = get_db_connect().cursor()
    _cursor.execute(f"""select * from usso.users 
            where token like '{_token}' """)
    _res = _cursor.fetchall()
    if len(_res) == 0:
        return False, f"Such token doesn't exist  ---> {_token}"

    # Check expire date
    if _res[0][2] < datetime.today().date():
        return False, f"Your token is out of date"
    return (True, _res[0][0], _token)


# write to log table
def write_req(_b, _cursor, _res, _token):
    try:
        _cursor.execute("""
            INSERT INTO usso.requests (
            token, Last_name ,First_name ,dob ,State, t_stamp, 
            founded, nickname )
            VALUES (%s,%s,%s,%s,%s,%s,%s, %s);""", (_token,
            _b['last_name'], _b['first_name'], _b['dob'],
            _b['state'], datetime.today(), _res, _b['nickname']));
    except:
        _cursor.execute("""
                   INSERT INTO usso.requests (
                   token, Last_name ,First_name ,dob, t_stamp, 
                   founded, nickname )
                   VALUES (%s,%s,%s,%s,%s,%s,%s);""", (_token,
                    _b['last_name'], _b['first_name'], _b['dob'],
                    datetime.today(), _res, _b['nickname']));
    return None


# filling the json, that'll be send
def make_rec(_rec):
    return {
        0: {"Last name": _rec[0]},
        1: {"First name": _rec[1]},
        2: {"Middle name": _rec[2]},
        3: {"Address": _rec[3]},
        4: {"Zip": _rec[4]},
        5: {"County": _rec[5]},
        6: {"State": _rec[6]},
        7: {"Gender": _rec[7]},
        8: {"Race": _rec[8]},
        9: {"DOB": _rec[9]},
        10: {"Height": _rec[10]},
        11: {"Eyes": _rec[11]},
        12: {"Hair color": _rec[12]},
        13: {"Weight": _rec[13]},
        14: {"Image": _rec[14]},
        15: {"Date of Conviction": _rec[15]},
        16: {"Conviction State": _rec[16]},
        17: {"Conviction": _rec[17]},
        18: {"Case Details": _rec[18]},
        19: {"URL": _rec[19]},
        20: {"ID": _rec[20]}
    }


# getting current age of offender
# 1965
def get_year(_dob):
    try:
        return datetime.strptime(
            _dob, '%m/%d/%Y').year
    except:
        return '0000'


def get_age(_dob):
    try:
        return datetime.today().year - datetime.strptime(
            _dob, '%m/%d/%Y').year
    except:
        return '0000'


def get_header():
    _header = ['last_name', 'first_name', 'dob', 'state', 'nickname',
               "Last name",
                  "First name",
    "Middle name",
    "Address",
    "Zip",
    "County",
    "State",
    "Gender",
    "Race",
    "DOB",
    "Height",
    "Eyes",
    "Hair color",
    "Weight",
    "Image",
    "Date of Conviction",
    "Conviction State",
    "Conviction",
    "Case Details",
    "URL",
    "ID"]
    return _header


def check_none(_result, _request_body):
    _counter = 0
    _gl_counter = 0
    _chunk = list()
    _i = 0
    for _record in _request_body['list']:
        try:
            _resul = list(_result[_i])
        except:
            break
        _request_body = list(_record.values())
        _request_body.append('```')
        _res00 = [_r.strftime("%m/%d/%Y, %H:%M")
                    if isinstance(_r, datetime) else _r
                    for _r in _record]
        _r_w = [' ' if _el is None else _el for _el in _resul]
        _chunk.append(_request_body + _r_w)
        _i += 1
    _recs = ['```'.join(_rec) for _rec in _chunk]
    return None


def create_temp_table(_name, _curs):
    _curs.execute(f"""
    CREATE TABLE IF NOT EXISTS usso.t{_name} (
        last_name_r VARCHAR(256),
        first_name_r VARCHAR(256),
        dob_r VARCHAR(256),
        state_r VARCHAR(27),
        nickname VARCHAR(27),
        Last_name VARCHAR(256),
        First_name VARCHAR(256),
        Middle_name VARCHAR(256),
        Address VARCHAR(1500),
        Zip VARCHAR(256),
        County VARCHAR(256),
        State VARCHAR(256),
        Gender VARCHAR(256),
        Race VARCHAR(256),
        DOB VARCHAR(256),
        Height VARCHAR(256),
        Eyes VARCHAR(256),
        Hair_color VARCHAR(256),
        Weight VARCHAR(256),
        Image VARCHAR(256),
        Date_of_Conviction VARCHAR(256),
        Conviction_State VARCHAR(256),
        Conviction VARCHAR(5000),
        Case_Details VARCHAR(1500),
        URL VARCHAR(256),
        ID VARCHAR(256)
        )
    """)
    return None


def spent_(_pr_time):
    _delta = time.time() - _pr_time
    if _delta >= 3600:
        _hours = int(_delta / 3600)
        _minutes = int((_delta - _hours *3600)/ 60)
        _seconds = _delta - _hours *3600 - _minutes *60
        return f'{_hours}h{_minutes}m{_seconds}s'
    if _delta > 60:
        _minutes = int(_delta / 60)
        _seconds = int(_delta - _minutes *60)
        return f'{_minutes}m{_seconds}s'
    else:
        return f'{int(_delta)}s'


def drop_temp_table(_name, _cursor):
    _cursor.execute(f"""drop table if exists usso.t{_name} """)
    return None


def norm_usso(_cursor):
    from tqdm import tqdm
    _cursor.execute("""select * from usso.usso 
            where dob like '%(DOB:%)';""")
    _records = _cursor.fetchall()
    with concurrent.futures.ThreadPoolExecutor(
            max_workers=4) as executor:
        _rels_submit = {executor.submit(pr_n_rec,
                                        _each, _cursor): _each
                        for _each in _records}
    for _future in tqdm(concurrent.futures.as_completed
                            (_rels_submit)):
        _res = _rels_submit[_future]
        try:
            _future.result()
        except Exception as _exc:

            print(_exc)
            print(traceback.format_exc())
        else:
            pass
    return None


def pr_n_rec(_rec, _cursor):
    _dob = _rec[9].split('(DOB:')[1].replace(')', '')
    _cursor.execute(f"""update usso.usso set dob = '{_dob}'
                where id like '{_rec[-2]}' """)
    return None


def main():

    import requests
    _cursor = get_db_connect().cursor()
    import time
    # norm_usso(_cursor)

    event = {"token": "some_token",

             "output_type": "json",
                          "list":
    [
    {"last_name": "GOUIG", "first_name": "Paul",
     "dob": "06/01/1949",
     "nickname": "F"},
    {"last_name": "MATHENIA", "first_name": "LONNIE",
     "dob": "12/21/1964", "nickname": "F"},
            {"last_name": "MARTIN",
                "first_name": "TERRY",
                "dob": "2/6/1960",
                "state": "OR"}]}

    _t = time.time()
    _cursor.execute("""
            select last_name, first_name, dob, state
                from usso.usso where state like 'OR' limit 5  """)
    _res = _cursor.fetchall()
    for _r in _res:
        event["list"].append({
             "last_name": _r[0],
                      "first_name": _r[1],
                      "dob": _r[2],
                        "state": _r[3],
                        'nickname': "F"})
    # check_none(_res, event)
    _rr = requests.post(endpoint, json=event)
    print(spent_(_t))
    _answer = json.loads(_rr.text)
    return None


if __name__ == '__main__':
    main()

'''update usso.usso set hair_color = '' WHERE hair_color is Null ;
update usso.usso set date_of_conviction = '' WHERE date_of_conviction is null;
update usso.usso set conviction_state = '' WHERE conviction_state is null;
update usso.usso set case_details = '' WHERE case_details r is null;
update usso.usso set dob = '' WHERE dob is null;
update usso.usso set conviction = '' WHERE conviction is null;
update usso.usso set address = '' WHERE address is null;
update usso.usso set zip = '' WHERE zip is null;
update usso.usso set county = '' WHERE county is null;
update usso.usso set gender = '' WHERE gender is null;
update usso.usso set race = '' WHERE race is null;
update usso.usso set height = '' WHERE height is null;
update usso.usso set eyes = '' WHERE eyes is null;
update usso.usso set date_of_conviction = '' WHERE date_of_conviction is null;
update usso.usso set dob = '' WHERE dob is null;
update usso.usso set weight = '' WHERE weight is null;
update usso.usso set image = '' WHERE image is null;
update usso.usso set last_name = '' WHERE last_name is null;
update usso.usso set first_name = '' WHERE first_name is null;
update usso.usso set middle_name = '' WHERE middle_name is null;
update usso.usso set state = '' WHERE state is null;
update usso.usso set url = '' WHERE url is null;'''
