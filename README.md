# US_SOR_ search_ app_AWS Lambda_API

The request to the US_SOR API must be carried out using the request post with the body as valid JSON with such keys:
API's endpoint is https://some_aws_endpoint

"last_name" - person's last name (strictly in capital letters);
"first_name" - persons's first name (strictly in capital letters);
"dob" - person's date of birth (strictly in mm/dd/YYYY format;
"state" - person's residence state;
"token" - your API token.

For example, a request to API with the following JSON body::
{'token': 'Your_token',
             'last_name': 'PHARES',
                      'first_name': 'STEPHEN',
                      'dob': '8/29/1961', 'state': 'IN'}
Had the following result:
{"0": {"Last name": "PHARES"}, 
"1": {"First name": "STEPHEN"}, 
"2": {"Middle name": "EUGENE"}, 
"3": {"Address": "7410 E 21ST ST"}, 
"4": {"Zip": ""}, "5": {"County": ""}, 
"6": {"State": "IN"}, 
"7": {"Gender": "M"}, 
"8": {"Race": "White"}, 
"9": {"DOB": "59"}, 
"10": {"Height": "6'00''"}, 
"11": {"Eyes": "Brown"}, 
"12": {"Hair color": "Brown"}, 
"13": {"Weight": "142lbs"}, 
"14": {"Image": "https://docs.watchsystems.com/pictures/54234/1426195-849ec669-eb28-49e6-b67e-48aed55446cd.jpg"}, 
"15": {"Date of Conviction": " 01/23/2003 "}, 
"16": {"Conviction State": " Indiana "}, 
"17": {"Conviction": " 35-42-4-3 - Child Molesting 10 YEARS"}, 
"18": {"Case Details": ""}, 
"19": {"URL": "https://www.icrimewatch.net/offenderdetails.php?OfndrID=1426195&AgencyID=54234"}, 
"20": {"ID": "1426195"}}

In rare cases of multiple matches, API will return an array of all matching JSONs.

Also output can be reached  in a JSON format instead of CSV:
In this case 'output_type': 'json' needs to be added to request's body, for example:

{'token': 'CbnxyeOmsVaqk6ynv2y5Ws-UMQ', 'output_type': 'json', 'list': [{'last_name': 'mateos-CERVANTES', 'first_name': 'Amparo', 'dob': '12/7/1966', 'state': 'IL', 'nickname': 'F'}, {'last_name': 'MATEY-VICENTE', 'first_name': 'TEODORO', 'dob': '7/18/1986', 'state': 'IL', 'nickname': 'F'}, {'last_name': 'MATHENIA', 'first_name': 'LONNIE', 'dob': '12/21/1964', 'state': 'IL', 'nickname': 'F'}, {'last_name': 'SHELTON', 'first_name': 'WESLEY', 'dob': '50', 'state': 'MO', 'nickname': 'F'}, {'last_name': 'SHIPP', 'first_name': 'ROBERT', 'dob': '33', 'state': 'MO', 'nickname': 'F'}]}

And output will be the list with such data schema:[
"Last name",
"First name",
"Middle name",
"Address",
"Zip",
"County",
"State",
"Gender",
"Race",
"DOB",
"Height",
"Eyes",
"Hair color",
"Weight",
"Image",
"Date of Conviction",
"Conviction State",
"Conviction",
"Case Details",
"URL",
"ID"]


